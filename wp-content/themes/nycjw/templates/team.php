<?php
/**
 * Template Name:  Team Page
 *
 * The template for displaying the team page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package NYCJW
 */
get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<section class="section">
				<div class="section-wrapper">
					<div id="page-header">
						<?php
						$titleWidth = get_title_length(get_the_title()); ?>
						<h1 class="section-title <?php echo $titleWidth; ?>">
							<?php the_title(); ?>
						</h1>
						<hr>
					</div>
					<div id="page-content">
						<?php the_content(); ?>
					</div>
					<div id="team-container">
						<?php
						$args = array(
					    'post_type' => 'team',
					    'posts_per_page' => -1,
							'orderby' => 'menu_order',
							'order' => 'ASC',
					    'tax_query' => [
								[
									'taxonomy' => 'member-type',
									'field' => 'slug',
									'terms' => 'primary',
									'operator' => 'IN',
								]
							]
					  );
						echo get_team_members($args);
						$args = array(
					    'post_type' => 'team',
					    'posts_per_page' => -1,
							'orderby' => 'menu_order',
							'order' => 'ASC',
							'tax_query' => [
								[
									'taxonomy' => 'member-type',
									'field' => 'slug',
									'terms' => 'primary',
									'operator' => 'NOT IN',
								]
							]
					  );
						echo get_team_members($args);
						?>
					</div>
				</div>
			</section>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
