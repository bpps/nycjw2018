<?php
/**
 * Template Name:  Events Page
 *
 * The template for displaying the events page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package NYCJW
 */
get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php
		$eventCount = 10;
		while ( have_posts() ) :
				the_post();
	      $args = array(
	        'post_type' => 'event',
					'post_status' => 'publish',
	        'posts_per_page' => $eventCount,
					'meta_key' => 'start_date',
	        'orderby' => 'meta_value',
	        'order' => 'ASC',
					'paged' => 1
	      );
				$eventdates = get_terms([
					'taxonomy' => 'event-date',
					'hide_empty' => true,
				]);
				$eventlocs = get_terms([
					'taxonomy' => 'event-location',
					'hide_empty' => true,
				]);
				$eventtypes = get_terms([
					'taxonomy' => 'event-type',
					'hide_empty' => true,
				]); ?>
				<section class="section">
					<div class="section-wrapper">
						<div id="page-header">
							<?php
							$titleWidth = get_title_length(get_the_title()); ?>
							<h1 class="section-title <?php echo $titleWidth; ?>">
								<?php the_title(); ?>
							</h1>
							<hr>
						</div>
						<div id="page-content">
							<?php the_content(); ?>
						</div>
						<div id="event-filter">
							<div id="event-filter-sentence">
								<div class="custom-select">
									<h3>Event Type</h3>
									<select class="event-select" id="event-type-select" data-param="type">
										<option value="all">Any event type</option>
										<?php
										foreach($eventtypes as $etype) { ?>
											<option value="<?php echo $etype->slug; ?>">
												<?php echo $etype->name; ?>
											</option>
										<?php
										} ?>
									</select>
								</div>
								<div class="custom-select" id="custom-date-select">
									<h3>Date</h3>
									<select class="event-select" id="event-date-select" data-param="date">
										<option value="all">Any date</option>
										<?php
										usort($eventdates, "sort_event_dates");
										foreach($eventdates as $edate) { ?>
											<option value="<?php echo $edate->slug; ?>">
												<?php echo $edate->name; ?>
											</option>
										<?php
										} ?>
									</select>
								</div>
								<div class="custom-select">
									<h3>Neighborhood</h3>
									<select class="event-select" id="event-location-select" data-param="location">
										<option value="all">Any neighborhood</option>
										<?php
										foreach($eventlocs as $eloc) { ?>
											<option value="<?php echo $eloc->slug; ?>">
												<?php echo $eloc->name; ?>
											</option>
										<?php
										} ?>
									</select>
								</div>
							</div>
							<a href="http://" target="_blank" id="clear-filter">Clear Filter</a>
						</div>
						<div id="events-container">
							<?php
							$events = get_event_posts(['event-date']);
							echo $events; ?>
						</div>
						<?php if($events->more){ ?>
							<a class="load-more" href="http://" data-date="<?php echo $events->date; ?>" data-paged="2" data-count="<?php echo $eventCount; ?>">
								LOAD MORE
							</a>
						<?php } ?>
					</div>
				</section>
			<?php
			endwhile; // End of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
