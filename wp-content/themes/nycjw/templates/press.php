<?php
/**
 * Template Name:  Press Page
 *
 * The template for displaying advisory page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package NYCJW
 */
get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php
		while ( have_posts() ) :
			the_post();
			$args = array(
		    'post_type' => 'press',
		    'posts_per_page' => -1,
				'orderby' => 'menu_order',
				'order' => 'ASC'
		  );
		  $press = new WP_Query($args);
		  if($press->have_posts()) : ?>
				<section class="section">
					<div class="section-wrapper">
						<div id="page-header">
							<?php
							$titleWidth = get_title_length(get_the_title()); ?>
							<h1 class="section-title <?php echo $titleWidth; ?>">
								<?php the_title(); ?>
							</h1>
							<hr>
						</div>
						<div id="page-content">
					    <div id="press-preview">
					      <?php
					      while($press->have_posts()): $press->the_post();
					        $logo = get_field('press_logo'); ?>
					        <div class="press-item">
										<div class="press-item-content">
						          <div class="press-logo">
						            <img src="<?php echo $logo['sizes']['small']; ?>"/>
						          </div>
						          <div class="press-content">
						            <span><h4><?php echo get_field('publication'); ?></h4></span>
						            <a target="_blank" href="<?php echo get_field('press_link'); ?>">
						              <span><?php the_title(); ?></span>
						            </a>
						          </div>
										</div>
										<?php if(get_the_content()) { ?>
											<div class="press-additional-content">
												<?php the_content(); ?>
											</div>
										<?php } ?>
					        </div>
					      <?php
					      endwhile; ?>
					    </div>
						</div>
					</div>
				</section>
		  <?php
		  endif;
		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
