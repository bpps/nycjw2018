<?php
/**
 * Template Name:  Map Page
 *
 * The template for displaying the map page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package NYCJW
 */
get_header();
while ( have_posts() ) :
	the_post();
	$pw = post_password_required(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<section class="section">
				<div class="section-wrapper map-wrapper<?php echo $pw ? ' password-protected' : ''; ?>">
					<?php the_content(); ?>
				</div>
			</section>
		</main><!-- #main -->
	</div><!-- #primary -->

	<?php
	endwhile;
get_footer();
