<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package NYCJW
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<div class="loader">
				<div class="lds-default"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
			</div>
			<div id="fullpage" class="hidden">
				<!-- <section class="section" id="section-home">
					<div class="section-wrapper">
						<div class="home-logo">
							<?php display_jw_logo(); ?>
						</div>
						<h2 class="home-subtitle">November 12th - 18th</h2>
					</div>
				</section> -->
				<?php
				$navItems = wp_get_nav_menu_items( 'header-menu' );
				$menuArray = [];
				foreach($navItems as $navItem) {
					$postID = $navItem->object_id;
					$postContent = get_post($postID);
					$title = get_the_title($postID);
					$hometitle = get_field('homepage_title', $postID);
					$content = $postContent->post_content;
					$content = apply_filters('the_content', $content);
					$content = str_replace(']]>', ']]&gt;', $content);
					$slug = slugify($navItem->title);
					$hasChildren = false;
					$children = get_posts(
						array(
    					'post_type' => 'page',
    					'post_parent' => $postID,
    					'orderby' => 'menu_order',
							'order' => 'ASC'
						)
					);
					if($children){
						$hasChildren = true;
					}
					if(!get_field('hide_on_homepage', $postID)) {
						$menuArray[] = $slug; ?>
						<section class="section" id="section-<?php echo $slug; ?>" data-anchor="<?php echo $slug; ?>">
							<?php
							if($slug == 'events') {
								if($gallery = get_field('events_gallery', $postID)) { ?>
									<div id="events-gallery-container">
										<div id="events-gallery">
											<?php
											$eventImgURLs = [];
											foreach($gallery as $key=>$eventImage) {
												if($key < 4) { ?>
													<div class="event-gallery-image-container">
														<div class="event-gallery-image bg-centered" style="background-image:url(<?php echo $eventImage['sizes']['medium']; ?>);">

															<div class="temp-event-image bg-centered"></div>
															<?php if($key == 3) { ?>
																<a id="homepage-event-link" href="<?php echo site_url('events'); ?>"><span>NYCJW Events</span></a>
															<?php } ?>
														</div>
													</div>
												<?php
												}
												array_push($eventImgURLs, $eventImage['sizes']['medium']);
											}
											wp_localize_script('nycjw-js', 'eventImages', $eventImgURLs );  ?>
										</div>
										<!-- <div class="events-arrow-left">
											<i></i>
										</div>
										<div class="events-arrow-right">
											<i></i>
										</div> -->
									</div>

								<?php
								}
							}
							?>
							<div class="section-wrapper">
								<div class="section-content">
									<?php
									if(isset($hometitle)) { ?>
										<?php
										$titleWidth = get_title_length($hometitle); ?>
										<h1 class="section-title <?php echo $titleWidth; ?>">
											<?php echo $hometitle; ?>
										</h1>
										<hr>
									<?php
									}
									if($homecontent = get_field('homepage_content', $postID)) {
										//$homecontent = apply_filters('the_content', $homecontent);
										//$homecontent = str_replace(']]>', ']]&gt;', $homecontent);
										echo $homecontent;
									} else {
										echo $content;
									}
									?>
								</div>
							</div>
						</section>
					<?php
					}
				}
				wp_localize_script('nycjw-js', 'menuItems', $menuArray ); ?>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
