<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package NYCJW
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<?php $tempdir = get_template_directory_uri(); ?>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="google-site-verification" content="sLhRfaVnLako9H0wkv0xHZZ8CzZ8GyN9IkZXZw1UAlk" />
	<link href="https://fonts.googleapis.com/css?family=Raleway:300,400,400i,500,500i,700" rel="stylesheet">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="apple-touch-icon" sizes="57x57" href="<?php echo $tempdir; ?>/images/favicon/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo $tempdir; ?>/images/favicon/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo $tempdir; ?>/images/favicon/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo $tempdir; ?>/images/favicon/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo $tempdir; ?>/images/favicon/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo $tempdir; ?>/images/favicon/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo $tempdir; ?>/images/favicon/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo $tempdir; ?>/images/favicon/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo $tempdir; ?>/images/favicon/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo $tempdir; ?>/images/favicon/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo $tempdir; ?>/images/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<?php echo $tempdir; ?>/images/favicon/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo $tempdir; ?>/images/favicon/favicon-16x16.png">
	<link rel="manifest" href="<?php echo $tempdir; ?>/images/favicon/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="<?php echo $tempdir; ?>/images/favicon/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">
	<?php wp_head(); ?>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-121636920-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-121636920-1');
	</script>

</head>

<body <?php body_class(); ?>>
<div id="page" class="site">

	<header id="masthead" class="site-header">
		<div id="header-content">
			<div class="mobile-logo">
				<?php
				$homeURL = get_home_url().'/#home';
				if(!is_front_page() && !is_home()) {
					$homeURL = get_home_url();
				} ?>
				<a href="<?php echo $homeURL; ?>">
					<?php display_mobile_logo(); ?>
				</a>
			</div>
			<div class="menu-toggle">
				<div class="menu-toggle-line"></div>
			</div>
		</div>
		<div class="menu-overlay">
			<div class="menu-container">
				<nav id="site-navigation" class="main-navigation">
					<ul id="anchor-menu">

						<?php
						$navItems = wp_get_nav_menu_items( 'header-menu' );
						$prevItem = '';
						foreach($navItems as $navItem) {
							$slughash = '#'.slugify($navItem->title);
							$anchorslug = slugify($navItem->title);
							if($navItem->menu_item_parent != 0) {
								$slughash = '#'.$prevItem.'/'.slugify($navItem->title);
								$anchorslug = $prevItem.'/'.slugify($navItem->title);
							}else{
								$prevItem = slugify($navItem->title);
								// Move this section out of 'if loop' to include subpages ?>
								<li>
									<a class="anchor-navigation" href="<?php echo $navItem->url; ?>">
										<?php echo $navItem->title; ?>
									</a>
								</li>
								<?php
								// End of section
							}
						} ?>
					</ul>
				</nav><!-- #site-navigation -->
				<div id="header-contact-info">
					<a class="volunteer-link" href="<?php echo home_url('shop'); ?>" target="_blank">SHOP</a>
					<a class="join-list" target="_blank" href="http://">Join the Mailing List</a>
					<a href="mailto:hello@nycjewelryweek.com">hello@nycjewelryweek.com</a>
					<div class="social-icons">
						<div class="social-icon">
							<a target="_blank" href="http://instagram.com/nycjewelryweek"><img src="<?php echo get_template_directory_uri(); ?>/images/instagram.png"/></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header><!-- #masthead -->

	<div id="content" class="site-content">
