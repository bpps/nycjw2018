jQuery.noConflict();
(function( $ ) {
  $(function() {
    var map;
    if(typeof eventsObj !== 'undefined') {
      var events = $.parseJSON(eventsObj);
      mapboxgl.accessToken = 'pk.eyJ1IjoiYnBia255IiwiYSI6ImNqbHBzaTZubjI4azcza3M2czJzem56cDIifQ.aMEPR8dk5e3Jkn52QzesUw';
      map = new mapboxgl.Map({
          container: 'nycjw-map',
          style: 'mapbox://styles/bpbkny/cjlpmcnpa7t812speb0hkee3q',
          center: [-74.0392709, 40.7590403],
          zoom: 13,
          interactive: true,
      });

      if(!map.loaded()) {

      }
      var nav = new mapboxgl.NavigationControl();
      map.addControl(nav, 'top-left');
      map.on('load', function() {
          // Add a GeoJSON source containing place coordinates and information.
          map.addSource("places", {
              "type": "geojson",
              "data": events
          });
          var bounds = new mapboxgl.LngLatBounds();
          events.features.forEach(function(marker, ind) {
            var el = document.createElement('div');
            el.setAttribute('id', 'marker-'+marker.properties.eid);
            $(el).on('click', function() {
              console.log(marker.properties.eid);
              showEventInfo(marker.properties.eid);
            })
            let eventAddress = encodeURIComponent(marker.properties.address);
            console.log(eventAddress);
            $.get( `https://api.mapbox.com/geocoding/v5/mapbox.places/${eventAddress}.json?types=address&access_token=pk.eyJ1IjoiYnBia255IiwiYSI6ImNqbHBzaTZubjI4azcza3M2czJzem56cDIifQ.aMEPR8dk5e3Jkn52QzesUw`, function( data ) {
              	console.log(data)
                el.className = 'marker';
                $(el).html(`
                  <span class="event-icon">${marker.properties.mapnumber}</span>
                  <div class="marker-info">
                    <div class="map-item-img bg-centered" style="background-image:url('${marker.properties.image}');">
                    </div>
                    <div class="map-item-info">
                      <p class="map-item-title">
                        <strong>
                          ${marker.properties.title}
                        </strong>
                      </p>
                      <span class="map-item-loc">
                        ${marker.properties.description}
                      </span>
                    </div>
                  </div>`);
                bounds.extend(data.features[0].center);
                // make a marker for each feature and add to the map
                //console.log(marker.geometry.coordinates, marker.properties.title);
                var $marker = new mapboxgl.Marker(el)
                .setLngLat(data.features[0].center)
                // .setPopup(new mapboxgl.Popup({ offset: 25 }).setHTML(`
                //   <a href="${marker.properties.link}">
                //     <div class="map-item-img bg-centered" style="background-image:url('${marker.properties.image}');">
                //     </div>
                //     <div class="map-item-info">
                //       <p class="map-item-title">
                //         ${marker.properties.title}
                //       </p>
                //       <span class="map-item-loc">
                //         ${marker.properties.description}
                //       </span>
                //     </div>
                //   </a>`))

                .addTo(map);
          	});

          });
          setTimeout(function() {
            map.fitBounds(bounds, { padding: 40 });
          }, 1000);


      });
    }
    function showEventInfo(id) {
      $.ajax({
        url: ajaxurl,
        method: 'post',
        type: 'json',
        data: {
          'action': 'do_ajax',
          'fn' : 'get_event_info',
          'eid' : id,
        }
      }).done( function (response) {
        response = $.parseJSON(response);
        $('#map-info-content').html(response);
        if(!$('#map-info').hasClass('active')) {
          $('#map-info').addClass('active');
          $('#map-info-content').slideDown();
        }
        $('.new-event').removeClass('new-event');
        setTimeout(function() {
          map.resize();
          // map.flyTo({
          //   // These options control the ending camera position: centered at
          //   // the target, at zoom level 9, and north up.
          //   center: [-74.0392709, 40.7590403],
          //
          //   // These options control the flight curve, making it move
          //   // slowly and zoom out almost completely before starting
          //   // to pan.
          //   speed: 0.05, // make the flying slow
          //   curve: .1, // change the speed at which it zooms out
          //
          //   // This can be any easing function: it takes a number between
          //   // 0 and 1 and returns another number between 0 and 1.
          //   easing: function (t) {
          //       return t;
          //   }
          // });
        }, 500);

      });
    }
  });
})(jQuery);
