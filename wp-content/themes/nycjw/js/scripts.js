jQuery.noConflict();
(function( $ ) {
  $(function() {
    var $slickInstance;
    var $eventsSlick;
    var eventImgInt;
    var totalImages = 0;
    var currentImgInd = 3;
    var eventImgCount;
    var eventImgCounter = 0;

    if($('.partner-list-container').length) {
      $('.partner-list-container').masonry({
        // options
        itemSelector: '.partner-list',
        columnWidth: '.partner-list',
        percentPosition: true,
        gutter: '.partner-list-gutter'
      });
    }
    // If events filter, get URL params and process events.
    if($('#event-filter').length) {
      updateEventsByParams();
    }

    $(document).ready(function() {
      if($('.event-type-container').length) {
        checkEventPosition();
      }
      $('body').addClass('loaded');
      //if($('body').hasClass('hide-intro')) {
        $('.hashtag-link, .site-branding').toggleClass('active');
      //}
      eventsToggle();
      if($('#fullpage').length) {
        console.log(menuItems);
        $('#fullpage').fullpage({
          anchors: menuItems,
          //menu: '#anchor-menu',
          loopHorizontal: false,
          scrollOverflow: true,
          sectionSelector: '.section',
          onLeave: function(index, nextIndex, direction){
            if($('body').hasClass('menu-active')){
              $('body').toggleClass('menu-active');
            }
            var sectionSlug = $('.fp-section').eq(nextIndex).data('anchor');
            if(sectionSlug === 'events') {
              startImageRotation();
            } else {
              clearInterval(eventImgInt);
            }
            if(nextIndex === 2) {
              $slickInstance.slick('slickPlay');
            }
            if($('.fp-section.active .fp-scrollable').length) {
              var IScroll = $('.fp-section.active').find('.fp-scrollable').data('iscrollInstance');
              IScroll.scrollTo(0, 0, 0);
            }
        	},
          afterLoad: function(origin, destination, direction) {
            $('#fullpage').removeClass('hidden');
            $('.loader').fadeOut('fast');
            if(origin === 'events') {
              startImageRotation();
            }
          }
          // afterLoad: function() {
          //   if($('#team-index').length) {
          //     $('#team-index .team-item').each(function(m, member){
          //       $(member).on('click', function(e) {
          //         e.preventDefault();
          //         var memberslug = $(this).data('name');
          //         var $memberDiv = $('.team-member-container[data-advisor="'+memberslug+'"]');
          //         console.log($('#section-advisory-committee .fp-scroller'));
          //         $('#section-advisory-committee .fp-scroller').animate({
          //           transform: 'translate(0px, -'+$memberDiv.offset().top+') translateZ(0px)'
          //         }, 400);
          //       });
          //     });
          //   }
          // }
        });
        $('.section').each(function(){
          var $thisSection = this;
          if($('.fp-next', $thisSection).length){
            var $nextSlide = $('.slide:first-child',$thisSection).next('.slide');
            var nextTitle = $('.section-title', $nextSlide).html();
            $('.fp-next', $thisSection).html(nextTitle);
          }
        });
        if($('.next-page-button').length) {
          $('.next-page-button').on('click', function() {
            $('#fullPage').fullpage.moveSectionDown();
          });
        }
      }
      if($('.testimonials-wrapper').length) {
        $slickInstance = $('.testimonials-wrapper').slick({
          'arrows': true,
          'slidesToShow': 1,
          'slidesToScroll': 1,
          'autoplay': true,
          'autoplaySpeed': 10000,
          'fade': true,
          'infinite': true,
          'prevArrow' : $('.testimonial-arrow-left'),
					'nextArrow' : $('.testimonial-arrow-right'),
        });
      }

      if($('#events-gallery').length ) {
        startImageRotation();
      }

      $('.menu-toggle').on('click', function() {
        $('body').toggleClass('menu-active');
      });

      function startImageRotation() {
        eventImgCount = $('.event-gallery-image').length;
        if(typeof eventImages != 'undefined') {
          totalImages = eventImages.length;
        }
        if(eventImgCount < totalImages) {
          var newImageCount = $('.event-gallery-image:visible').length;
          eventImgInt = setInterval(function(){
            newImageCount = $('.event-gallery-image:visible').length;
            var currentEventImg = $('.event-gallery-image').eq(eventImgCounter);
            var currentImg = eventImages[currentImgInd];
            $('.temp-event-image', currentEventImg).css('background-image', 'url('+currentImg+')').addClass('active');
            setTimeout(function() {
              $(currentEventImg).css('background-image', 'url('+currentImg+')');
              $('.temp-event-image', currentEventImg).removeClass('active');
            }, 400);
            eventImgCounter = (eventImgCounter === eventImgCount - 1)
              ? 0
              : eventImgCounter+1;
            currentImgInd = (currentImgInd === totalImages -1)
              ? 0
              : currentImgInd+1;

          }, newImageCount * 500);
        }
      }

      if($('#event-filter').length) {
        $('.event-select').on('change', function(){
          $('.event-type-container').removeClass('active');
          var selectParam = $(this).data('param');
          var selectVal = $( this ).val();
          var newVal = selectVal;
          if(selectVal === 'all') {
            window.history.replaceState('', '', removeParameter(window.location.href, selectParam));
          } else {
            window.history.replaceState('', '', updateURLParameter(window.location.href, selectParam, selectVal));
          }
          updateEventsByParams();

        });

        // Clear filter button
        $('#clear-filter').on('click', function(e) {
          e.preventDefault();
          $('.event-select').each(function(){
            $(this).val('all');
          });
          var tempArray = window.location.href.split("?");
          var baseURL = tempArray[0];
          window.history.replaceState('', '', baseURL);
          updateEventsByParams();
        });

      }
    });

    function updateEventsByParams() {
      $('.event-type-container').show().addClass('visible');
      $('.event-item').show();
      $('option').removeAttr('disabled')
      var tempArray = window.location.href.split("?");
      var baseURL = tempArray[0];
      var paramURL = tempArray[1];
      if(paramURL) { // filter by params
        $('#clear-filter').addClass('active');
        var paramArray = paramURL.split("&");
        for (var i=0; i<paramArray.length; i++) {
          var paramStr = paramArray[i].split('=');
          var param = paramStr[0];
          var paramVal = paramStr[1];
          // Set select values (this only needs to happen on load but may as well just do it here...?)
          if($('#event-'+param+'-select').val() != paramVal) {
            $('#event-'+param+'-select').val(paramVal);
          };
          switch(param) {
            case 'date':
              $('.event-type-container').each(function(){
                if($(this).data('date') != paramVal && $(this).data('date') != 'ongoing-events') {
                  $(this).hide().removeClass('visible');
                } else { // This is the selected day

                }
              });
            break;
            default:
              $('.event-item').each(function(){
                if($(this).data(param) != paramVal) {
                  $(this).hide();
                }
              });
              $('.event-type-container').each(function(){
                var eventsRemaining = false;
                var eventContainerSlug = $(this).data('date');
                var eventContainer = $(this);
                $('.event-item', eventContainer).each(function(){
                  if($(this).is(':visible')) {
                    eventsRemaining = true;
                  }
                });
                if(!eventsRemaining) {
                  $(eventContainer).hide().removeClass('visible');
                  $('option[value='+eventContainerSlug+']').attr('disabled', 'disabled');
                }
              });
            break;
          }
        }
      } else { // show all events
        $('#clear-filter').removeClass('active');
      }
      var totalHeight = 0;
      setTimeout(function(){
        $('.event-type-container.visible').each(function(){
          if(totalHeight < $(window).height()) {
            $(this).addClass('active');
            totalHeight += $(this).height();
          }
        });
      }, 200);

    }

    function removeParameter(url, param) {
      var tempArray = url.split("?");
      var baseURL = tempArray[0];
      var paramURL = tempArray[1];
      var paramArray = paramURL.split("&");
      var newParamURL = '';
      var temp = '';
      if(paramArray.length > 1) {
        for (var i=0; i<paramArray.length; i++) {
          if(paramArray[i].split('=')[0] != param) {
            newParamURL += temp + paramArray[i];
            temp = '&';
          }
        }
        return baseURL + '?' + newParamURL;
      } else {
        return baseURL;
      }
    }

    function updateURLParameter(url, param, paramVal){
        var newAdditionalURL = "";
        var tempArray = url.split("?");
        var baseURL = tempArray[0];
        var additionalURL = tempArray[1];
        var temp = "";
        if (additionalURL) {
            tempArray = additionalURL.split("&");
            for (var i=0; i<tempArray.length; i++){
                if(tempArray[i].split('=')[0] != param){
                    newAdditionalURL += temp + tempArray[i];
                    temp = "&";
                }
            }
        }

        var rows_txt = temp + "" + param + "=" + paramVal;
        return baseURL + "?" + newAdditionalURL + rows_txt;
    }

    function eventsToggle() {
      if($('.new-event .desc-toggle').length) {
        $('.new-event .event-title').on('click', function(e){
          e.preventDefault();
          var $eventParent = $(this).closest('.event-item');
          if($($eventParent).hasClass('more-info')) {
            $($eventParent).removeClass('more-info');
            $('.event-desc', $eventParent).slideUp();
          } else {
            $($eventParent).addClass('more-info');
            $('.event-desc', $eventParent).slideDown(300, function(){

            });
            $('html, body').animate({
              scrollTop: $(this).closest('.event-item').offset().top -75
            });
            window.location.hash = $($eventParent).data('permalink');
          }

          window.dispatchEvent(new Event('resize'));
        });
        $('.event-type-container:first-of-type').addClass('active');
        if($('.event-type-container:first-of-type').height() < $(window).height()) {
          $('.event-type-container:nth-child(2)').addClass('active');
        }
        //setTimeout(function() {
          $('.new-event').each(function(){
            $(this).removeClass('new-event');
          });
        //}, 200);
        if ( window.location.hash ) {
          let hash = window.location.hash.substring(1);
          if($('#event-'+hash).length) {
            $('#event-'+hash+' .event-title').click();
          }
        }
      }
    }

    function loadMoreEvents($el, postsAdded) {
      var $el = $('.load-more');
      var paged = $($el).data('paged');
      var postCount = $($el).data('count');
      var currentDay = $($el).data('date');
      // var filterArray = null;
      // if($('.filter-btn').length) {
      //   filterArray = [];
      //   $('.filter-btn.active').each(function(){
      //     if($(this).data('filter') != 'all') {
      //       filterArray.push($(this).data('filter'));
      //     }
      //   });
      // }
      $.ajax({
        url: ajaxurl,
        method: 'post',
        type: 'json',
        data: {
          'action': 'do_ajax',
          'fn' : 'get_more_events',
          'paged' : paged,
          'count' : postCount,
          'currentday' : currentDay,
          //'filter' : filterArray
        }
      }).done( function (response) {
        response = $.parseJSON(response);
        if(response.posts) {
          $('#events-container').append(response.posts);
          $($el).data('paged', paged+1);
          $($el).data('date', response.date);
          eventsToggle();
          postsAdded(response.more);
        }
        if(!response.more) {
          $('.load-more').hide();
        }
      });
    }
    var loading = false;

    $(window).scroll(function(){
      // if(loading == false) {
      //   var toScroll = $(document).height() - $(window).height() - 100;
      //   console.log(toScroll, 'scroll',$(this).scrollTop());
      //   if ( $(this).scrollTop() > toScroll ) {
      //     loading = true;
      //     loadMoreEvents($('.load-more'), function(more){ if(more){loading = false}; });
      //   }
      // }
      if($('.event-type-container').length) {
        checkEventPosition();
      }

    });

    function checkEventPosition() {
      $('.event-type-container').each(function() {
        var $theseEvents = $(this);
        if(!$($theseEvents).hasClass('active')) {
          var toScroll = $(document).scrollTop() + $(window).height() - 100;
          if($($theseEvents).offset().top < toScroll) {
            $($theseEvents).addClass('active');
          }
        }
      });
    }

    if($('.join-list').length){
      $('.join-list').on('click', function(e) {
        e.preventDefault();
        $('body').addClass('signup-active');
      });
    }
    if($('#close-newsletter').length) {
      $('#close-newsletter').on('click', function(e){
        e.preventDefault();
        $('body').removeClass('signup-active');
        $('.jw-email').val('');
      });
    }

    if($('.jw-submit').length){
      $('.jw-submit').on('click', function(e) {
        e.preventDefault();
        $('.jw-email').removeClass('error');
        var email = $('.jw-email').val();
        if(email.length){
          $('#newsletter-container').addClass('loading');
          signup_user(email);
        } else {
          $('.jw-email').addClass('error');
        }
      });
    }

    function signup_user(email) {
      $.ajax({
        url: ajaxurl,
        method: 'post',
        type: 'json',
        data: {
          'action': 'do_ajax',
          'fn' : 'sign_up_user',
          'email' : email
        }
      }).done( function (response) {
        $('#newsletter-container').removeClass('loading');
        response = $.parseJSON(response);
        $('#newsletter-signup-text').html('<div class="response-text">'+response.response+'</div>');
        $('.jw-email').val('');
      });
    }

    function isEmail(email) {
      var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
      return regex.test(email);
    }

    //styleSelect();
    function styleSelect() {
      var x, i, j, selElmnt, a, b, c;
      /*look for any elements with the class "custom-select":*/
      x = document.getElementsByClassName("custom-select");
      for (i = 0; i < x.length; i++) {
        if($('.select-selected', x[i]).length == 0) {

          selElmnt = x[i].getElementsByTagName("select")[0];
          /*for each element, create a new DIV that will act as the selected item:*/
          a = document.createElement("DIV");
          a.setAttribute("class", "select-selected");
          a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
          x[i].appendChild(a);
          /*for each element, create a new DIV that will contain the option list:*/
          b = document.createElement("DIV");
          b.setAttribute("class", "select-items select-hide");
          for (j = 1; j < selElmnt.length; j++) {
            /*for each option in the original select element,
            create a new DIV that will act as an option item:*/
            c = document.createElement("DIV");
            c.innerHTML = selElmnt.options[j].innerHTML;
            c.addEventListener("click", function(e) {
                /*when an item is clicked, update the original select box,
                and the selected item:*/
                var y, i, k, s, h;
                s = this.parentNode.parentNode.getElementsByTagName("select")[0];
                h = this.parentNode.previousSibling;
                for (i = 0; i < s.length; i++) {
                  if (s.options[i].innerHTML == this.innerHTML) {
                    s.selectedIndex = i;
                    h.innerHTML = this.innerHTML;
                    y = this.parentNode.getElementsByClassName("same-as-selected");
                    for (k = 0; k < y.length; k++) {
                      y[k].removeAttribute("class");
                    }
                    this.setAttribute("class", "same-as-selected");
                    break;
                  }
                }
                h.click();
            });
            b.appendChild(c);
          }
          x[i].appendChild(b);
          a.addEventListener("click", function(e) {
              /*when the select box is clicked, close any other select boxes,
              and open/close the current select box:*/
              e.stopPropagation();
              closeAllSelect(this);
              this.nextSibling.classList.toggle("select-hide");
              this.classList.toggle("select-arrow-active");
          });
        }
      }
      function closeAllSelect(elmnt) {
        /*a function that will close all select boxes in the document,
        except the current select box:*/
        var x, y, i, arrNo = [];
        x = document.getElementsByClassName("select-items");
        y = document.getElementsByClassName("select-selected");
        for (i = 0; i < y.length; i++) {
          if (elmnt == y[i]) {
            arrNo.push(i)
          } else {
            y[i].classList.remove("select-arrow-active");
          }
        }
        for (i = 0; i < x.length; i++) {
          if (arrNo.indexOf(i)) {
            x[i].classList.add("select-hide");
          }
        }
      }
      /*if the user clicks anywhere outside the select box,
      then close all select boxes:*/
      document.addEventListener("click", closeAllSelect);
    }

  });
})(jQuery);
