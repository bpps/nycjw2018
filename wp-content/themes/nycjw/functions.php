<?php
/**
 * NYCJW functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package NYCJW
 */

if ( ! function_exists( 'nycjw_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function nycjw_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on NYCJW, use a find and replace
		 * to change 'nycjw' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'nycjw', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		add_image_size( 'small', 400 );
		add_image_size( 'small-medium', 600 );
		add_image_size( 'medium', 800 );
		add_image_size( 'large', 1400 );
		add_image_size( 'extra-large', 1900 );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'nycjw' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'nycjw_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'nycjw_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function nycjw_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'nycjw_content_width', 640 );
}
add_action( 'after_setup_theme', 'nycjw_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function nycjw_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'nycjw' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'nycjw' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'nycjw_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function nycjw_scripts() {

	$manifest = json_decode(file_get_contents('dist/assets.json', true));
	$main = $manifest->main;

	wp_enqueue_style( 'nycjw-style', get_template_directory_uri() . $main->css, false, null );

	wp_enqueue_script('nycjw-js', get_template_directory_uri() . $main->js, ['jquery'], null, true);

	wp_enqueue_style( 'fullpage-style', get_template_directory_uri() .'/js/fullpage/jquery.fullPage.css',false, null);

	wp_enqueue_script( 'fullpage-script', get_template_directory_uri() .'/js/fullpage/jquery.fullPage.js', ['jquery'], null, true);

	wp_enqueue_script( 'fullpage-overflow', get_template_directory_uri() .'/js/fullpage/vendors/scrolloverflow.min.js', ['jquery'], null, true);

	wp_enqueue_style( 'slick-style', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css' );

	wp_enqueue_script( 'slick', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js' );

	wp_enqueue_script( 'masonry', 'https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js' );

	//wp_enqueue_script( 'nycjw-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	//wp_enqueue_script( 'nycjw-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	wp_enqueue_script( 'map-box-script', 'https://api.tiles.mapbox.com/mapbox-gl-js/v0.48.0/mapbox-gl.js', array(), null, true );

	wp_enqueue_style( 'map-box-style', 'https://api.tiles.mapbox.com/mapbox-gl-js/v0.48.0/mapbox-gl.css', false, null);

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'nycjw_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
	* NYCJ Shortcodes.
	*/
require get_template_directory() . '/inc/nycjw-shortcodes.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

if( function_exists('acf_add_options_page') ) {

	acf_add_options_page(array(
		'page_title' 	=> 'Settings',
		'menu_title'	=> 'NYCJW Settings',
		'menu_slug' 	=> 'nycjw-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Testimonials',
		'menu_title'	=> 'Testimonials',
		'parent_slug'	=> 'nycjw-settings',
	));

}

function slugify($text)
{
  $text = preg_replace('~[^\pL\d]+~u', '-', $text);
  $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
  $text = preg_replace('~[^-\w]+~', '', $text);
  $text = trim($text, '-');
  $text = preg_replace('~-+~', '-', $text);
  $text = strtolower($text);

  if (empty($text)) {
    return 'n-a';
  }

  return $text;
}

function register_custom_post_types() {

	$labels = array(
    'name' => _x('Team', 'post type general name'),
    'singular_name' => _x('Team Member', 'post type singular name'),
    'add_new' => _x('Add New', 'team'),
    'add_new_item' => __('Add New Team Member'),
    'edit_item' => __('Edit Team Member'),
    'new_item' => __('New Team Member'),
    'view_item' => __('View Team Member'),
    'search_items' => __('Search Team'),
    'not_found' =>  __('Nothing found'),
    'not_found_in_trash' => __('Nothing found in Trash'),
    'parent_item_colon' => ''
  );

  $args = array(
    'labels' => $labels,
    'public' => false,
    'publicly_queryable' => false,
    'exclude_from_search' => true,
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => true,
    'capability_type' => 'post',
    'hierarchical' => true,
    'menu_position' => 5,
    'has_archive' => false,
    'supports' => array('title', 'editor', 'thumbnail', 'excerpt', 'page-attributes'),
    'taxonomies' => array('member-type'),
  );

  register_post_type( 'team' , $args );

	$labels = array(
    'name' => _x('Advisors', 'post type general name'),
    'singular_name' => _x('Advisor', 'post type singular name'),
    'add_new' => _x('Add New', 'advisor'),
    'add_new_item' => __('Add New Advisor'),
    'edit_item' => __('Edit Advisor'),
    'new_item' => __('New Advisor'),
    'view_item' => __('View Advisor'),
    'search_items' => __('Search Advisors'),
    'not_found' =>  __('Nothing found'),
    'not_found_in_trash' => __('Nothing found in Trash'),
    'parent_item_colon' => ''
  );

  $args = array(
    'labels' => $labels,
    'public' => false,
    'publicly_queryable' => false,
    'exclude_from_search' => true,
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => true,
    'capability_type' => 'post',
    'hierarchical' => true,
    'menu_position' => 5,
    'has_archive' => false,
    'supports' => array('title', 'editor', 'thumbnail', 'excerpt'),
    'taxonomies' => array('advisor-type'),
  );

  register_post_type( 'advisor' , $args );

	$labels = array(
    'name' => _x('Press', 'post type general name'),
    'singular_name' => _x('Press', 'post type singular name'),
    'add_new' => _x('Add New', 'press'),
    'add_new_item' => __('Add New Press'),
    'edit_item' => __('Edit Press'),
    'new_item' => __('New Press'),
    'view_item' => __('View Press'),
    'search_items' => __('Search Press'),
    'not_found' =>  __('Nothing found'),
    'not_found_in_trash' => __('Nothing found in Trash'),
    'parent_item_colon' => ''
  );

  $args = array(
    'labels' => $labels,
    'public' => false,
    'publicly_queryable' => false,
    'exclude_from_search' => true,
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => true,
    'capability_type' => 'post',
    'hierarchical' => false,
    'menu_position' => 5,
    'has_archive' => false,
    'supports' => array('title', 'editor', 'page-attributes'),
    'taxonomies' => '',
  );

  register_post_type( 'press' , $args );

	$labels = array(
    'name' => _x('Events', 'post type general name'),
    'singular_name' => _x('Event', 'post type singular name'),
    'add_new' => _x('Add New', 'event'),
    'add_new_item' => __('Add New Event'),
    'edit_item' => __('Edit Event'),
    'new_item' => __('New Event'),
    'view_item' => __('View Event'),
    'search_items' => __('Search Events'),
    'not_found' =>  __('Nothing found'),
    'not_found_in_trash' => __('Nothing found in Trash'),
    'parent_item_colon' => ''
  );

  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'exclude_from_search' => true,
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => true,
    'capability_type' => 'post',
    'hierarchical' => true,
    'menu_position' => 5,
    'has_archive' => false,
    'supports' => array('title', 'editor', 'thumbnail', 'excerpt'),
    'taxonomies' => array('event-type', 'event-location'),
  );

  register_post_type( 'event' , $args );

	$labels = array(
    'name' => _x('Emerging Creatives', 'post type general name'),
    'singular_name' => _x('Emerging Creative', 'post type singular name'),
    'add_new' => _x('Add New', 'emerging-creatives'),
    'add_new_item' => __('Add New Emerging Creative'),
    'edit_item' => __('Edit Emerging Creative'),
    'new_item' => __('New Emerging Creative'),
    'view_item' => __('View Emerging Creative'),
    'search_items' => __('Search Emerging Creatives'),
    'not_found' =>  __('Nothing found'),
    'not_found_in_trash' => __('Nothing found in Trash'),
    'parent_item_colon' => ''
  );

  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'exclude_from_search' => true,
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => true,
    'capability_type' => 'post',
    'hierarchical' => true,
    'menu_position' => 5,
    'has_archive' => false,
    'supports' => array('title', 'editor', 'thumbnail', 'excerpt'),
    //'taxonomies' => array('advisor-type'),
  );

  register_post_type( 'emerging-creatives' , $args );
}

add_action('init', 'register_custom_post_types');

function register_custom_taxonomies() {

	$labels = array(
		'name'                       => _x( 'Member Types', 'taxonomy general name', 'nycjw' ),
		'singular_name'              => _x( 'Member Type', 'taxonomy singular name', 'nycjw' ),
		'search_items'               => __( 'Search Member Types', 'nycjw' ),
		'popular_items'              => __( 'Popular Member Types', 'nycjw' ),
		'all_items'                  => __( 'All Member Types', 'nycjw' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __( 'Edit Member Type', 'nycjw' ),
		'update_item'                => __( 'Update Member Type', 'nycjw' ),
		'add_new_item'               => __( 'Add New Member Type', 'nycjw' ),
		'new_item_name'              => __( 'New Member Type Name', 'nycjw' ),
		'separate_items_with_commas' => __( 'Separate member types with commas', 'nycjw' ),
		'add_or_remove_items'        => __( 'Add or remove member types', 'nycjw' ),
		'choose_from_most_used'      => __( 'Choose from the most used member types', 'nycjw' ),
		'not_found'                  => __( 'No member types found.', 'nycjw' ),
		'menu_name'                  => __( 'Member Types', 'nycjw' ),
	);

	$args = array(

    'show_tagcloud'         => true,
		'hierarchical'          => true,
		'labels'                => $labels,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'show_in_nav_menus'     => true,
		'public'								=> true,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		'rewrite'               => array( 'slug' => 'advisor-type' ),
	);

	register_taxonomy( 'member-type', array('team'), $args );
	register_taxonomy_for_object_type( 'member-type', array('team') );

	$labels = array(
		'name'                       => _x( 'Advisor Types', 'taxonomy general name', 'nycjw' ),
		'singular_name'              => _x( 'Advisor Type', 'taxonomy singular name', 'nycjw' ),
		'search_items'               => __( 'Search Advisor Types', 'nycjw' ),
		'popular_items'              => __( 'Popular Advisor Types', 'nycjw' ),
		'all_items'                  => __( 'All Advisor Types', 'nycjw' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __( 'Edit Advisor Type', 'nycjw' ),
		'update_item'                => __( 'Update Advisor Type', 'nycjw' ),
		'add_new_item'               => __( 'Add New Advisor Type', 'nycjw' ),
		'new_item_name'              => __( 'New Advisor Type Name', 'nycjw' ),
		'separate_items_with_commas' => __( 'Separate advisor types with commas', 'nycjw' ),
		'add_or_remove_items'        => __( 'Add or remove advisor types', 'nycjw' ),
		'choose_from_most_used'      => __( 'Choose from the most used advisor types', 'nycjw' ),
		'not_found'                  => __( 'No advisor types found.', 'nycjw' ),
		'menu_name'                  => __( 'Advisor Types', 'nycjw' ),
	);

	$args = array(

    'show_tagcloud'         => true,
		'hierarchical'          => true,
		'labels'                => $labels,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'show_in_nav_menus'     => true,
		'public'								=> true,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		'rewrite'               => array( 'slug' => 'advisor-type' ),
	);

	register_taxonomy( 'advisor-type', array('advisor'), $args );
	register_taxonomy_for_object_type( 'advisor-type', array('advisor') );

	$labels = array(
		'name'                       => _x( 'Event Types', 'taxonomy general name', 'nycjw' ),
		'singular_name'              => _x( 'Event Type', 'taxonomy singular name', 'nycjw' ),
		'search_items'               => __( 'Search Event Types', 'nycjw' ),
		'popular_items'              => __( 'Popular Event Types', 'nycjw' ),
		'all_items'                  => __( 'All Event Types', 'nycjw' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __( 'Edit Event Type', 'nycjw' ),
		'update_item'                => __( 'Update Event Type', 'nycjw' ),
		'add_new_item'               => __( 'Add New Event Type', 'nycjw' ),
		'new_item_name'              => __( 'New Event Type', 'nycjw' ),
		'separate_items_with_commas' => __( 'Separate event types with commas', 'nycjw' ),
		'add_or_remove_items'        => __( 'Add or remove event types', 'nycjw' ),
		'choose_from_most_used'      => __( 'Choose from the most used event types', 'nycjw' ),
		'not_found'                  => __( 'No event types found.', 'nycjw' ),
		'menu_name'                  => __( 'Event Types', 'nycjw' ),
	);

	$args = array(

    'show_tagcloud'         => true,
		'hierarchical'          => true,
		'labels'                => $labels,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'show_in_nav_menus'     => true,
		'public'								=> true,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		'rewrite'               => array( 'slug' => 'event-type' ),
	);

	register_taxonomy( 'event-type', array('event'), $args );
	register_taxonomy_for_object_type( 'event-type', array('event') );

	$labels = array(
		'name'                       => _x( 'Event Locations', 'taxonomy general name', 'nycjw' ),
		'singular_name'              => _x( 'Event Location', 'taxonomy singular name', 'nycjw' ),
		'search_items'               => __( 'Search Event Locations', 'nycjw' ),
		'popular_items'              => __( 'Popular Event Locations', 'nycjw' ),
		'all_items'                  => __( 'All Event Locations', 'nycjw' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __( 'Edit Event Location', 'nycjw' ),
		'update_item'                => __( 'Update Event Location', 'nycjw' ),
		'add_new_item'               => __( 'Add New Event Location', 'nycjw' ),
		'new_item_name'              => __( 'New Event Location', 'nycjw' ),
		'separate_items_with_commas' => __( 'Separate event locations with commas', 'nycjw' ),
		'add_or_remove_items'        => __( 'Add or remove event locations', 'nycjw' ),
		'choose_from_most_used'      => __( 'Choose from the most used event locations', 'nycjw' ),
		'not_found'                  => __( 'No event locationss found.', 'nycjw' ),
		'menu_name'                  => __( 'Event Locations', 'nycjw' ),
	);

	$args = array(

    'show_tagcloud'         => true,
		'hierarchical'          => true,
		'labels'                => $labels,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'show_in_nav_menus'     => true,
		'public'								=> true,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		'rewrite'               => array( 'slug' => 'event-location' ),
	);

	register_taxonomy( 'event-location', array('event'), $args );
	register_taxonomy_for_object_type( 'event-location', array('event') );

	$labels = array(
		'name'                       => _x( 'Event Dates', 'taxonomy general name', 'nycjw' ),
		'singular_name'              => _x( 'Event Date', 'taxonomy singular name', 'nycjw' ),
		'search_items'               => __( 'Search Event Dates', 'nycjw' ),
		'popular_items'              => __( 'Popular Event Dates', 'nycjw' ),
		'all_items'                  => __( 'All Event Dates', 'nycjw' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __( 'Edit Event Date', 'nycjw' ),
		'update_item'                => __( 'Update Event Date', 'nycjw' ),
		'add_new_item'               => __( 'Add New Event Date', 'nycjw' ),
		'new_item_name'              => __( 'New Event Date', 'nycjw' ),
		'separate_items_with_commas' => __( 'Separate event dates with commas', 'nycjw' ),
		'add_or_remove_items'        => __( 'Add or remove event dates', 'nycjw' ),
		'choose_from_most_used'      => __( 'Choose from the most used event dates', 'nycjw' ),
		'not_found'                  => __( 'No event dates found.', 'nycjw' ),
		'menu_name'                  => __( 'Event Dates', 'nycjw' ),
	);

	$args = array(

    'show_tagcloud'         => true,
		'hierarchical'          => true,
		'labels'                => $labels,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'show_in_nav_menus'     => true,
		'public'								=> true,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		'rewrite'               => array( 'slug' => 'event-date' ),
	);

	register_taxonomy( 'event-date', array('event'), $args );
	register_taxonomy_for_object_type( 'event-date', array('event') );
}

add_action('init', 'register_custom_taxonomies', 0);

function filter_get_terms_orderby( $orderby, $this_query_vars, $this_query_vars_taxonomy ) {
    // make filter magic happen here...
    return 'slug';
};

// add the filter
add_filter( 'get_terms_orderby', 'filter_get_terms_orderby', 10, 3 );

function remove_protected_title_prefix() {
	return '%s';
}

add_filter( 'protected_title_format', 'remove_protected_title_prefix', 10, 3);

function custom_password_form($post) {
  $post = get_post( $post );
  $label = 'pwbox-' . ( empty($post->ID) ? rand() : $post->ID );
  $output = '<form action="' . esc_url( site_url( 'wp-login.php?action=postpass', 'login_post' ) ) . '" class="post-password-form" method="post">
  <p>' . __( 'This content is password protected. To view it please enter your password below' ) . '</p>
  <div id="password-form-fields"><input name="post_password" id="' . $label . '" placeholder="password" type="password" size="20" /> <input type="submit" name="Submit" value="' . esc_attr_x( 'Enter', 'post password form' ) . '" /></div></form>
  ';
  return $output;
}

add_filter( 'the_password_form', 'custom_password_form', 10, 3);


function order_acf_event_date_taxonomy( $args, $field )
{
    // run the_content filter on all textarea values
		$args['orderby'] = 'slug';
		//$args['order'] = 'DESC';

    return $args;
}

// acf/load_value - filter for every value load
add_filter('acf/fields/taxonomy/query/name=event_date', 'order_acf_event_date_taxonomy', 10, 3);

function add_acf_columns ( $columns ) {
	return array_merge ( $columns, array (
   'publication' => __ ( 'Publication' )
 	) );
}

add_filter ( 'manage_press_posts_columns', 'add_acf_columns' );

function press_custom_column ( $column, $post_id ) {
	switch ( $column ) {
		case 'publication':
			echo get_post_meta ( $post_id, 'publication', true );
			break;
	}
}
add_action ( 'manage_press_posts_custom_column', 'press_custom_column', 10, 2 );
