/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/dist/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__(1);
__webpack_require__(2);
__webpack_require__(3);
//require('slick-carousel')
__webpack_require__(4);

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
var __WEBPACK_LOCAL_MODULE_0__, __WEBPACK_LOCAL_MODULE_0__factory, __WEBPACK_LOCAL_MODULE_0__module;var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

/*!
 * imagesLoaded PACKAGED v4.1.4
 * JavaScript is all like "You images are done yet or what?"
 * MIT License
 */

!function (e, t) {
   true ? !(__WEBPACK_LOCAL_MODULE_0__factory = (t), (__WEBPACK_LOCAL_MODULE_0__module = { id: "ev-emitter/ev-emitter", exports: {}, loaded: false }), __WEBPACK_LOCAL_MODULE_0__ = (typeof __WEBPACK_LOCAL_MODULE_0__factory === 'function' ? (__WEBPACK_LOCAL_MODULE_0__factory.call(__WEBPACK_LOCAL_MODULE_0__module.exports, __webpack_require__, __WEBPACK_LOCAL_MODULE_0__module.exports, __WEBPACK_LOCAL_MODULE_0__module)) : __WEBPACK_LOCAL_MODULE_0__factory), (__WEBPACK_LOCAL_MODULE_0__module.loaded = true), __WEBPACK_LOCAL_MODULE_0__ === undefined && (__WEBPACK_LOCAL_MODULE_0__ = __WEBPACK_LOCAL_MODULE_0__module.exports)) : "object" == (typeof module === "undefined" ? "undefined" : _typeof(module)) && module.exports ? module.exports = t() : e.EvEmitter = t();
}("undefined" != typeof window ? window : undefined, function () {
  function e() {}var t = e.prototype;return t.on = function (e, t) {
    if (e && t) {
      var i = this._events = this._events || {},
          n = i[e] = i[e] || [];return n.indexOf(t) == -1 && n.push(t), this;
    }
  }, t.once = function (e, t) {
    if (e && t) {
      this.on(e, t);var i = this._onceEvents = this._onceEvents || {},
          n = i[e] = i[e] || {};return n[t] = !0, this;
    }
  }, t.off = function (e, t) {
    var i = this._events && this._events[e];if (i && i.length) {
      var n = i.indexOf(t);return n != -1 && i.splice(n, 1), this;
    }
  }, t.emitEvent = function (e, t) {
    var i = this._events && this._events[e];if (i && i.length) {
      i = i.slice(0), t = t || [];for (var n = this._onceEvents && this._onceEvents[e], o = 0; o < i.length; o++) {
        var r = i[o],
            s = n && n[r];s && (this.off(e, r), delete n[r]), r.apply(this, t);
      }return this;
    }
  }, t.allOff = function () {
    delete this._events, delete this._onceEvents;
  }, e;
}), function (e, t) {
  "use strict";
   true ? !(__WEBPACK_AMD_DEFINE_ARRAY__ = [__WEBPACK_LOCAL_MODULE_0__], __WEBPACK_AMD_DEFINE_RESULT__ = (function (i) {
    return t(e, i);
  }).apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__)) : "object" == (typeof module === "undefined" ? "undefined" : _typeof(module)) && module.exports ? module.exports = t(e, require("ev-emitter")) : e.imagesLoaded = t(e, e.EvEmitter);
}("undefined" != typeof window ? window : undefined, function (e, t) {
  function i(e, t) {
    for (var i in t) {
      e[i] = t[i];
    }return e;
  }function n(e) {
    if (Array.isArray(e)) return e;var t = "object" == (typeof e === "undefined" ? "undefined" : _typeof(e)) && "number" == typeof e.length;return t ? d.call(e) : [e];
  }function o(e, t, r) {
    if (!(this instanceof o)) return new o(e, t, r);var s = e;return "string" == typeof e && (s = document.querySelectorAll(e)), s ? (this.elements = n(s), this.options = i({}, this.options), "function" == typeof t ? r = t : i(this.options, t), r && this.on("always", r), this.getImages(), h && (this.jqDeferred = new h.Deferred()), void setTimeout(this.check.bind(this))) : void a.error("Bad element for imagesLoaded " + (s || e));
  }function r(e) {
    this.img = e;
  }function s(e, t) {
    this.url = e, this.element = t, this.img = new Image();
  }var h = e.jQuery,
      a = e.console,
      d = Array.prototype.slice;o.prototype = Object.create(t.prototype), o.prototype.options = {}, o.prototype.getImages = function () {
    this.images = [], this.elements.forEach(this.addElementImages, this);
  }, o.prototype.addElementImages = function (e) {
    "IMG" == e.nodeName && this.addImage(e), this.options.background === !0 && this.addElementBackgroundImages(e);var t = e.nodeType;if (t && u[t]) {
      for (var i = e.querySelectorAll("img"), n = 0; n < i.length; n++) {
        var o = i[n];this.addImage(o);
      }if ("string" == typeof this.options.background) {
        var r = e.querySelectorAll(this.options.background);for (n = 0; n < r.length; n++) {
          var s = r[n];this.addElementBackgroundImages(s);
        }
      }
    }
  };var u = { 1: !0, 9: !0, 11: !0 };return o.prototype.addElementBackgroundImages = function (e) {
    var t = getComputedStyle(e);if (t) for (var i = /url\((['"])?(.*?)\1\)/gi, n = i.exec(t.backgroundImage); null !== n;) {
      var o = n && n[2];o && this.addBackground(o, e), n = i.exec(t.backgroundImage);
    }
  }, o.prototype.addImage = function (e) {
    var t = new r(e);this.images.push(t);
  }, o.prototype.addBackground = function (e, t) {
    var i = new s(e, t);this.images.push(i);
  }, o.prototype.check = function () {
    function e(e, i, n) {
      setTimeout(function () {
        t.progress(e, i, n);
      });
    }var t = this;return this.progressedCount = 0, this.hasAnyBroken = !1, this.images.length ? void this.images.forEach(function (t) {
      t.once("progress", e), t.check();
    }) : void this.complete();
  }, o.prototype.progress = function (e, t, i) {
    this.progressedCount++, this.hasAnyBroken = this.hasAnyBroken || !e.isLoaded, this.emitEvent("progress", [this, e, t]), this.jqDeferred && this.jqDeferred.notify && this.jqDeferred.notify(this, e), this.progressedCount == this.images.length && this.complete(), this.options.debug && a && a.log("progress: " + i, e, t);
  }, o.prototype.complete = function () {
    var e = this.hasAnyBroken ? "fail" : "done";if (this.isComplete = !0, this.emitEvent(e, [this]), this.emitEvent("always", [this]), this.jqDeferred) {
      var t = this.hasAnyBroken ? "reject" : "resolve";this.jqDeferred[t](this);
    }
  }, r.prototype = Object.create(t.prototype), r.prototype.check = function () {
    var e = this.getIsImageComplete();return e ? void this.confirm(0 !== this.img.naturalWidth, "naturalWidth") : (this.proxyImage = new Image(), this.proxyImage.addEventListener("load", this), this.proxyImage.addEventListener("error", this), this.img.addEventListener("load", this), this.img.addEventListener("error", this), void (this.proxyImage.src = this.img.src));
  }, r.prototype.getIsImageComplete = function () {
    return this.img.complete && this.img.naturalWidth;
  }, r.prototype.confirm = function (e, t) {
    this.isLoaded = e, this.emitEvent("progress", [this, this.img, t]);
  }, r.prototype.handleEvent = function (e) {
    var t = "on" + e.type;this[t] && this[t](e);
  }, r.prototype.onload = function () {
    this.confirm(!0, "onload"), this.unbindEvents();
  }, r.prototype.onerror = function () {
    this.confirm(!1, "onerror"), this.unbindEvents();
  }, r.prototype.unbindEvents = function () {
    this.proxyImage.removeEventListener("load", this), this.proxyImage.removeEventListener("error", this), this.img.removeEventListener("load", this), this.img.removeEventListener("error", this);
  }, s.prototype = Object.create(r.prototype), s.prototype.check = function () {
    this.img.addEventListener("load", this), this.img.addEventListener("error", this), this.img.src = this.url;var e = this.getIsImageComplete();e && (this.confirm(0 !== this.img.naturalWidth, "naturalWidth"), this.unbindEvents());
  }, s.prototype.unbindEvents = function () {
    this.img.removeEventListener("load", this), this.img.removeEventListener("error", this);
  }, s.prototype.confirm = function (e, t) {
    this.isLoaded = e, this.emitEvent("progress", [this, this.element, t]);
  }, o.makeJQueryPlugin = function (t) {
    t = t || e.jQuery, t && (h = t, h.fn.imagesLoaded = function (e, t) {
      var i = new o(this, e, t);return i.jqDeferred.promise(h(this));
    });
  }, o.makeJQueryPlugin(), o;
});

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


jQuery.noConflict();
(function ($) {
  $(function () {
    var map;
    if (typeof eventsObj !== 'undefined') {
      var events = $.parseJSON(eventsObj);
      mapboxgl.accessToken = 'pk.eyJ1IjoiYnBia255IiwiYSI6ImNqbHBzaTZubjI4azcza3M2czJzem56cDIifQ.aMEPR8dk5e3Jkn52QzesUw';
      map = new mapboxgl.Map({
        container: 'nycjw-map',
        style: 'mapbox://styles/bpbkny/cjlpmcnpa7t812speb0hkee3q',
        center: [-74.0392709, 40.7590403],
        zoom: 13,
        interactive: true
      });

      if (!map.loaded()) {}
      var nav = new mapboxgl.NavigationControl();
      map.addControl(nav, 'top-left');
      map.on('load', function () {
        // Add a GeoJSON source containing place coordinates and information.
        map.addSource("places", {
          "type": "geojson",
          "data": events
        });
        var bounds = new mapboxgl.LngLatBounds();
        events.features.forEach(function (marker, ind) {
          var el = document.createElement('div');
          el.setAttribute('id', 'marker-' + marker.properties.eid);
          $(el).on('click', function () {
            console.log(marker.properties.eid);
            showEventInfo(marker.properties.eid);
          });
          var eventAddress = encodeURIComponent(marker.properties.address);
          console.log(eventAddress);
          $.get('https://api.mapbox.com/geocoding/v5/mapbox.places/' + eventAddress + '.json?types=address&access_token=pk.eyJ1IjoiYnBia255IiwiYSI6ImNqbHBzaTZubjI4azcza3M2czJzem56cDIifQ.aMEPR8dk5e3Jkn52QzesUw', function (data) {
            console.log(data);
            el.className = 'marker';
            $(el).html('\n                  <span class="event-icon">' + marker.properties.mapnumber + '</span>\n                  <div class="marker-info">\n                    <div class="map-item-img bg-centered" style="background-image:url(\'' + marker.properties.image + '\');">\n                    </div>\n                    <div class="map-item-info">\n                      <p class="map-item-title">\n                        <strong>\n                          ' + marker.properties.title + '\n                        </strong>\n                      </p>\n                      <span class="map-item-loc">\n                        ' + marker.properties.description + '\n                      </span>\n                    </div>\n                  </div>');
            bounds.extend(data.features[0].center);
            // make a marker for each feature and add to the map
            //console.log(marker.geometry.coordinates, marker.properties.title);
            var $marker = new mapboxgl.Marker(el).setLngLat(data.features[0].center)
            // .setPopup(new mapboxgl.Popup({ offset: 25 }).setHTML(`
            //   <a href="${marker.properties.link}">
            //     <div class="map-item-img bg-centered" style="background-image:url('${marker.properties.image}');">
            //     </div>
            //     <div class="map-item-info">
            //       <p class="map-item-title">
            //         ${marker.properties.title}
            //       </p>
            //       <span class="map-item-loc">
            //         ${marker.properties.description}
            //       </span>
            //     </div>
            //   </a>`))

            .addTo(map);
          });
        });
        setTimeout(function () {
          map.fitBounds(bounds, { padding: 40 });
        }, 1000);
      });
    }
    function showEventInfo(id) {
      $.ajax({
        url: ajaxurl,
        method: 'post',
        type: 'json',
        data: {
          'action': 'do_ajax',
          'fn': 'get_event_info',
          'eid': id
        }
      }).done(function (response) {
        response = $.parseJSON(response);
        $('#map-info-content').html(response);
        if (!$('#map-info').hasClass('active')) {
          $('#map-info').addClass('active');
          $('#map-info-content').slideDown();
        }
        $('.new-event').removeClass('new-event');
        setTimeout(function () {
          map.resize();
          // map.flyTo({
          //   // These options control the ending camera position: centered at
          //   // the target, at zoom level 9, and north up.
          //   center: [-74.0392709, 40.7590403],
          //
          //   // These options control the flight curve, making it move
          //   // slowly and zoom out almost completely before starting
          //   // to pan.
          //   speed: 0.05, // make the flying slow
          //   curve: .1, // change the speed at which it zooms out
          //
          //   // This can be any easing function: it takes a number between
          //   // 0 and 1 and returns another number between 0 and 1.
          //   easing: function (t) {
          //       return t;
          //   }
          // });
        }, 500);
      });
    }
  });
})(jQuery);

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


jQuery.noConflict();
(function ($) {
  $(function () {
    var $slickInstance;
    var $eventsSlick;
    var eventImgInt;
    var totalImages = 0;
    var currentImgInd = 3;
    var eventImgCount;
    var eventImgCounter = 0;

    if ($('.partner-list-container').length) {
      $('.partner-list-container').masonry({
        // options
        itemSelector: '.partner-list',
        columnWidth: '.partner-list',
        percentPosition: true,
        gutter: '.partner-list-gutter'
      });
    }
    // If events filter, get URL params and process events.
    if ($('#event-filter').length) {
      updateEventsByParams();
    }

    $(document).ready(function () {
      if ($('.event-type-container').length) {
        checkEventPosition();
      }
      $('body').addClass('loaded');
      //if($('body').hasClass('hide-intro')) {
      $('.hashtag-link, .site-branding').toggleClass('active');
      //}
      eventsToggle();
      if ($('#fullpage').length) {
        console.log(menuItems);
        $('#fullpage').fullpage({
          anchors: menuItems,
          //menu: '#anchor-menu',
          loopHorizontal: false,
          scrollOverflow: true,
          sectionSelector: '.section',
          onLeave: function onLeave(index, nextIndex, direction) {
            if ($('body').hasClass('menu-active')) {
              $('body').toggleClass('menu-active');
            }
            var sectionSlug = $('.fp-section').eq(nextIndex).data('anchor');
            if (sectionSlug === 'events') {
              startImageRotation();
            } else {
              clearInterval(eventImgInt);
            }
            if (nextIndex === 2) {
              $slickInstance.slick('slickPlay');
            }
            if ($('.fp-section.active .fp-scrollable').length) {
              var IScroll = $('.fp-section.active').find('.fp-scrollable').data('iscrollInstance');
              IScroll.scrollTo(0, 0, 0);
            }
          },
          afterLoad: function afterLoad(origin, destination, direction) {
            $('#fullpage').removeClass('hidden');
            $('.loader').fadeOut('fast');
            if (origin === 'events') {
              startImageRotation();
            }
          }
          // afterLoad: function() {
          //   if($('#team-index').length) {
          //     $('#team-index .team-item').each(function(m, member){
          //       $(member).on('click', function(e) {
          //         e.preventDefault();
          //         var memberslug = $(this).data('name');
          //         var $memberDiv = $('.team-member-container[data-advisor="'+memberslug+'"]');
          //         console.log($('#section-advisory-committee .fp-scroller'));
          //         $('#section-advisory-committee .fp-scroller').animate({
          //           transform: 'translate(0px, -'+$memberDiv.offset().top+') translateZ(0px)'
          //         }, 400);
          //       });
          //     });
          //   }
          // }
        });
        $('.section').each(function () {
          var $thisSection = this;
          if ($('.fp-next', $thisSection).length) {
            var $nextSlide = $('.slide:first-child', $thisSection).next('.slide');
            var nextTitle = $('.section-title', $nextSlide).html();
            $('.fp-next', $thisSection).html(nextTitle);
          }
        });
        if ($('.next-page-button').length) {
          $('.next-page-button').on('click', function () {
            $('#fullPage').fullpage.moveSectionDown();
          });
        }
      }
      if ($('.testimonials-wrapper').length) {
        $slickInstance = $('.testimonials-wrapper').slick({
          'arrows': true,
          'slidesToShow': 1,
          'slidesToScroll': 1,
          'autoplay': true,
          'autoplaySpeed': 10000,
          'fade': true,
          'infinite': true,
          'prevArrow': $('.testimonial-arrow-left'),
          'nextArrow': $('.testimonial-arrow-right')
        });
      }

      if ($('#events-gallery').length) {
        startImageRotation();
      }

      $('.menu-toggle').on('click', function () {
        $('body').toggleClass('menu-active');
      });

      function startImageRotation() {
        eventImgCount = $('.event-gallery-image').length;
        if (typeof eventImages != 'undefined') {
          totalImages = eventImages.length;
        }
        if (eventImgCount < totalImages) {
          var newImageCount = $('.event-gallery-image:visible').length;
          eventImgInt = setInterval(function () {
            newImageCount = $('.event-gallery-image:visible').length;
            var currentEventImg = $('.event-gallery-image').eq(eventImgCounter);
            var currentImg = eventImages[currentImgInd];
            $('.temp-event-image', currentEventImg).css('background-image', 'url(' + currentImg + ')').addClass('active');
            setTimeout(function () {
              $(currentEventImg).css('background-image', 'url(' + currentImg + ')');
              $('.temp-event-image', currentEventImg).removeClass('active');
            }, 400);
            eventImgCounter = eventImgCounter === eventImgCount - 1 ? 0 : eventImgCounter + 1;
            currentImgInd = currentImgInd === totalImages - 1 ? 0 : currentImgInd + 1;
          }, newImageCount * 500);
        }
      }

      if ($('#event-filter').length) {
        $('.event-select').on('change', function () {
          $('.event-type-container').removeClass('active');
          var selectParam = $(this).data('param');
          var selectVal = $(this).val();
          var newVal = selectVal;
          if (selectVal === 'all') {
            window.history.replaceState('', '', removeParameter(window.location.href, selectParam));
          } else {
            window.history.replaceState('', '', updateURLParameter(window.location.href, selectParam, selectVal));
          }
          updateEventsByParams();
        });

        // Clear filter button
        $('#clear-filter').on('click', function (e) {
          e.preventDefault();
          $('.event-select').each(function () {
            $(this).val('all');
          });
          var tempArray = window.location.href.split("?");
          var baseURL = tempArray[0];
          window.history.replaceState('', '', baseURL);
          updateEventsByParams();
        });
      }
    });

    function updateEventsByParams() {
      $('.event-type-container').show().addClass('visible');
      $('.event-item').show();
      $('option').removeAttr('disabled');
      var tempArray = window.location.href.split("?");
      var baseURL = tempArray[0];
      var paramURL = tempArray[1];
      if (paramURL) {
        // filter by params
        $('#clear-filter').addClass('active');
        var paramArray = paramURL.split("&");
        for (var i = 0; i < paramArray.length; i++) {
          var paramStr = paramArray[i].split('=');
          var param = paramStr[0];
          var paramVal = paramStr[1];
          // Set select values (this only needs to happen on load but may as well just do it here...?)
          if ($('#event-' + param + '-select').val() != paramVal) {
            $('#event-' + param + '-select').val(paramVal);
          };
          switch (param) {
            case 'date':
              $('.event-type-container').each(function () {
                if ($(this).data('date') != paramVal && $(this).data('date') != 'ongoing-events') {
                  $(this).hide().removeClass('visible');
                } else {// This is the selected day

                }
              });
              break;
            default:
              $('.event-item').each(function () {
                if ($(this).data(param) != paramVal) {
                  $(this).hide();
                }
              });
              $('.event-type-container').each(function () {
                var eventsRemaining = false;
                var eventContainerSlug = $(this).data('date');
                var eventContainer = $(this);
                $('.event-item', eventContainer).each(function () {
                  if ($(this).is(':visible')) {
                    eventsRemaining = true;
                  }
                });
                if (!eventsRemaining) {
                  $(eventContainer).hide().removeClass('visible');
                  $('option[value=' + eventContainerSlug + ']').attr('disabled', 'disabled');
                }
              });
              break;
          }
        }
      } else {
        // show all events
        $('#clear-filter').removeClass('active');
      }
      var totalHeight = 0;
      setTimeout(function () {
        $('.event-type-container.visible').each(function () {
          if (totalHeight < $(window).height()) {
            $(this).addClass('active');
            totalHeight += $(this).height();
          }
        });
      }, 200);
    }

    function removeParameter(url, param) {
      var tempArray = url.split("?");
      var baseURL = tempArray[0];
      var paramURL = tempArray[1];
      var paramArray = paramURL.split("&");
      var newParamURL = '';
      var temp = '';
      if (paramArray.length > 1) {
        for (var i = 0; i < paramArray.length; i++) {
          if (paramArray[i].split('=')[0] != param) {
            newParamURL += temp + paramArray[i];
            temp = '&';
          }
        }
        return baseURL + '?' + newParamURL;
      } else {
        return baseURL;
      }
    }

    function updateURLParameter(url, param, paramVal) {
      var newAdditionalURL = "";
      var tempArray = url.split("?");
      var baseURL = tempArray[0];
      var additionalURL = tempArray[1];
      var temp = "";
      if (additionalURL) {
        tempArray = additionalURL.split("&");
        for (var i = 0; i < tempArray.length; i++) {
          if (tempArray[i].split('=')[0] != param) {
            newAdditionalURL += temp + tempArray[i];
            temp = "&";
          }
        }
      }

      var rows_txt = temp + "" + param + "=" + paramVal;
      return baseURL + "?" + newAdditionalURL + rows_txt;
    }

    function eventsToggle() {
      if ($('.new-event .desc-toggle').length) {
        $('.new-event .event-title').on('click', function (e) {
          e.preventDefault();
          var $eventParent = $(this).closest('.event-item');
          if ($($eventParent).hasClass('more-info')) {
            $($eventParent).removeClass('more-info');
            $('.event-desc', $eventParent).slideUp();
          } else {
            $($eventParent).addClass('more-info');
            $('.event-desc', $eventParent).slideDown(300, function () {});
            $('html, body').animate({
              scrollTop: $(this).closest('.event-item').offset().top - 75
            });
            window.location.hash = $($eventParent).data('permalink');
          }

          window.dispatchEvent(new Event('resize'));
        });
        $('.event-type-container:first-of-type').addClass('active');
        if ($('.event-type-container:first-of-type').height() < $(window).height()) {
          $('.event-type-container:nth-child(2)').addClass('active');
        }
        //setTimeout(function() {
        $('.new-event').each(function () {
          $(this).removeClass('new-event');
        });
        //}, 200);
        if (window.location.hash) {
          var hash = window.location.hash.substring(1);
          if ($('#event-' + hash).length) {
            $('#event-' + hash + ' .event-title').click();
          }
        }
      }
    }

    function loadMoreEvents($el, postsAdded) {
      var $el = $('.load-more');
      var paged = $($el).data('paged');
      var postCount = $($el).data('count');
      var currentDay = $($el).data('date');
      // var filterArray = null;
      // if($('.filter-btn').length) {
      //   filterArray = [];
      //   $('.filter-btn.active').each(function(){
      //     if($(this).data('filter') != 'all') {
      //       filterArray.push($(this).data('filter'));
      //     }
      //   });
      // }
      $.ajax({
        url: ajaxurl,
        method: 'post',
        type: 'json',
        data: {
          'action': 'do_ajax',
          'fn': 'get_more_events',
          'paged': paged,
          'count': postCount,
          'currentday': currentDay
          //'filter' : filterArray
        }
      }).done(function (response) {
        response = $.parseJSON(response);
        if (response.posts) {
          $('#events-container').append(response.posts);
          $($el).data('paged', paged + 1);
          $($el).data('date', response.date);
          eventsToggle();
          postsAdded(response.more);
        }
        if (!response.more) {
          $('.load-more').hide();
        }
      });
    }
    var loading = false;

    $(window).scroll(function () {
      // if(loading == false) {
      //   var toScroll = $(document).height() - $(window).height() - 100;
      //   console.log(toScroll, 'scroll',$(this).scrollTop());
      //   if ( $(this).scrollTop() > toScroll ) {
      //     loading = true;
      //     loadMoreEvents($('.load-more'), function(more){ if(more){loading = false}; });
      //   }
      // }
      if ($('.event-type-container').length) {
        checkEventPosition();
      }
    });

    function checkEventPosition() {
      $('.event-type-container').each(function () {
        var $theseEvents = $(this);
        if (!$($theseEvents).hasClass('active')) {
          var toScroll = $(document).scrollTop() + $(window).height() - 100;
          if ($($theseEvents).offset().top < toScroll) {
            $($theseEvents).addClass('active');
          }
        }
      });
    }

    if ($('.join-list').length) {
      $('.join-list').on('click', function (e) {
        e.preventDefault();
        $('body').addClass('signup-active');
      });
    }
    if ($('#close-newsletter').length) {
      $('#close-newsletter').on('click', function (e) {
        e.preventDefault();
        $('body').removeClass('signup-active');
        $('.jw-email').val('');
      });
    }

    if ($('.jw-submit').length) {
      $('.jw-submit').on('click', function (e) {
        e.preventDefault();
        $('.jw-email').removeClass('error');
        var email = $('.jw-email').val();
        if (email.length) {
          $('#newsletter-container').addClass('loading');
          signup_user(email);
        } else {
          $('.jw-email').addClass('error');
        }
      });
    }

    function signup_user(email) {
      $.ajax({
        url: ajaxurl,
        method: 'post',
        type: 'json',
        data: {
          'action': 'do_ajax',
          'fn': 'sign_up_user',
          'email': email
        }
      }).done(function (response) {
        $('#newsletter-container').removeClass('loading');
        response = $.parseJSON(response);
        $('#newsletter-signup-text').html('<div class="response-text">' + response.response + '</div>');
        $('.jw-email').val('');
      });
    }

    function isEmail(email) {
      var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
      return regex.test(email);
    }

    //styleSelect();
    function styleSelect() {
      var x, i, j, selElmnt, a, b, c;
      /*look for any elements with the class "custom-select":*/
      x = document.getElementsByClassName("custom-select");
      for (i = 0; i < x.length; i++) {
        if ($('.select-selected', x[i]).length == 0) {

          selElmnt = x[i].getElementsByTagName("select")[0];
          /*for each element, create a new DIV that will act as the selected item:*/
          a = document.createElement("DIV");
          a.setAttribute("class", "select-selected");
          a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
          x[i].appendChild(a);
          /*for each element, create a new DIV that will contain the option list:*/
          b = document.createElement("DIV");
          b.setAttribute("class", "select-items select-hide");
          for (j = 1; j < selElmnt.length; j++) {
            /*for each option in the original select element,
            create a new DIV that will act as an option item:*/
            c = document.createElement("DIV");
            c.innerHTML = selElmnt.options[j].innerHTML;
            c.addEventListener("click", function (e) {
              /*when an item is clicked, update the original select box,
              and the selected item:*/
              var y, i, k, s, h;
              s = this.parentNode.parentNode.getElementsByTagName("select")[0];
              h = this.parentNode.previousSibling;
              for (i = 0; i < s.length; i++) {
                if (s.options[i].innerHTML == this.innerHTML) {
                  s.selectedIndex = i;
                  h.innerHTML = this.innerHTML;
                  y = this.parentNode.getElementsByClassName("same-as-selected");
                  for (k = 0; k < y.length; k++) {
                    y[k].removeAttribute("class");
                  }
                  this.setAttribute("class", "same-as-selected");
                  break;
                }
              }
              h.click();
            });
            b.appendChild(c);
          }
          x[i].appendChild(b);
          a.addEventListener("click", function (e) {
            /*when the select box is clicked, close any other select boxes,
            and open/close the current select box:*/
            e.stopPropagation();
            closeAllSelect(this);
            this.nextSibling.classList.toggle("select-hide");
            this.classList.toggle("select-arrow-active");
          });
        }
      }
      function closeAllSelect(elmnt) {
        /*a function that will close all select boxes in the document,
        except the current select box:*/
        var x,
            y,
            i,
            arrNo = [];
        x = document.getElementsByClassName("select-items");
        y = document.getElementsByClassName("select-selected");
        for (i = 0; i < y.length; i++) {
          if (elmnt == y[i]) {
            arrNo.push(i);
          } else {
            y[i].classList.remove("select-arrow-active");
          }
        }
        for (i = 0; i < x.length; i++) {
          if (arrNo.indexOf(i)) {
            x[i].classList.add("select-hide");
          }
        }
      }
      /*if the user clicks anywhere outside the select box,
      then close all select boxes:*/
      document.addEventListener("click", closeAllSelect);
    }
  });
})(jQuery);

/***/ }),
/* 4 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ })
/******/ ]);
//# sourceMappingURL=main.js.map