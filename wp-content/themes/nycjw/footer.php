<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package NYCJW
 */

?>

	</div><!-- #content -->
	<footer id="colophon" class="site-footer">
		<div id="footer-wrapper">
		<div id="contact-info-wrapper">
			<div id="contact-info">
				<!-- <div class="row-1">
					<a class="volunteer-link" href="https://nycjewelryweek.com/shop" target="_blank">SHOP</a>
				</div> -->
				<div class="row-2">
					<!-- <a href="mailto:hello@nycjewelryweek.com">hello@nycjewelryweek.com</a> -->
					<a class="volunteer-link" href="https://nycjewelryweek.com">Return to 2019</a>
					<div class="social-icons">
						<div class="social-icon">
							<a target="_blank" href="http://instagram.com/nycjewelryweek"><img src="<?php echo get_template_directory_uri(); ?>/images/instagram.png"/></a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="footer-right">

		</div>
		<div class="site-branding">
			<div class="site-branding-wrapper">
				<?php
				$homeURL = get_home_url().'/#home';
				if(!is_front_page() && !is_home()) {
					$homeURL = get_home_url();
				} ?>
				<a class="anchor-navigation" id="small-logo" href="<?php echo $homeURL; ?>" rel="home">
					<?php display_jw_logo_circle(); ?>
				</a>
				<?php
				the_custom_logo();
				if ( !is_front_page() && !is_home() ) : ?>
					<a id="small-logo" href="<?php echo is_home() || is_front_page() ? '' : home_url().'#home'; ?>" rel="home">
						<!-- <img src="<?php echo get_template_directory_uri(); ?>/images/logo-small.png"/> -->
					</a>
				<?php
				endif; ?>
			</div>
		</div><!-- .site-branding -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<div id="fixed-background">
	<!-- <div class="yellow-bg">
	</div> -->
	<div class="circle-overlay">
	</div>
</div>

<?php
$ref = preg_replace('#^https?://#', '', trim(wp_get_referer()));
$homeurl = preg_replace('#^https?://#', '', trim(home_url('/')));
//if ($ref !== '' && strpos(parse_url($ref), parse_url($homeurl)) !== false) {
if (!is_home() && !is_front_page() || $ref !== '' && strpos(parse_url($ref), parse_url($homeurl)) !== false) {
  // Internal URL ?>
	<style>
		body.loaded .site-footer {
			transition-delay: .5s!important;
		}
	</style>
	<?php
}else{
	// External URL ?>
	<div class="intro-overlay">
		<div class="intro-logo-container">
			<div class="intro-logo">
				<?php display_jw_logo(); ?>
				<!-- <h2 class="home-subtitle">November 18th - 24th, 2019</h2> -->
			</div>
		</div>
	</div>
<?php
} ?>

<section id="newsletter-container">
	<div id="newsletter-wrapper">
		<div id="newsletter-content">
			<a href="http://" id="close-newsletter">
			</a>
			<div id="newsletter-image">
				<img src="<?php echo get_template_directory_uri(); ?>/images/mail-logo.png"/>
			</div>
			<div id="newsletter-signup">
				<div id="newsletter-signup-text">
					<h1><span>Stay in the Loop!</span></h1>
					<p>Sign up to receive the latest updates  from NYC Jewelry Week in your inbox.</p>
				</div>
				<form>
					<input class="jw-email" type="email" placeholder="email"/>
					<div class="jw-submit-wrapper">
						<div class="jw-submit-loader">
							<div class="lds-default"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
						</div>
						<input class="jw-submit" type="submit" value="sign up"/>
					</div>
				</form>
			</div>

		</div>
	</div>
</section>

<?php wp_footer(); ?>

</body>
</html>
