<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package NYCJW
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php
		while ( have_posts() ) :
			the_post(); ?>
			<section class="section">
				<div class="section-wrapper">
					<div id="page-header">
						<?php
						if(has_post_thumbnail()) { ?>
							<div class="ec-featured-image">
								<?php the_post_thumbnail('medium'); ?>
							</div>
						<?php } ?>
						<div class="ec-content">
							<div class="ec-title">
								<h1>
									<?php
									the_title();
									?>
								</h1>
								<?php
								if($location = get_field('ec_location')) { ?>
									<p><?php echo $location; ?></p>
								<?php
								} ?>
							</div>
							<div id="ec-links">
								<?php
								if($website = get_field('ec_website')) { ?>
									<a target="_blank" href="<?php echo $website; ?>">
										<img src="<?php echo get_template_directory_uri(); ?>/images/website.png"/>
									</a>
								<?php
								}
								if($email = get_field('ec_email')) { ?>
									<a href="mailto:<?php echo $email; ?>">
										<img src="<?php echo get_template_directory_uri(); ?>/images/email.png"/>
									</a>
								<?php
								}
								if($insta = get_field('ec_instagram')) { ?>
									<a target="_blank" href="https://www.instagram.com/<?php echo $insta; ?>">
										<img src="<?php echo get_template_directory_uri(); ?>/images/instagram.png"/>
										<span>@<?php echo $insta; ?></span>
									</a>
								<?php
								}
								?>
							</div>
							<div class="ec-description">
								<?php the_content(); ?>
							</div>
						</div>
					</div>
				</div>
			</section>
		<?php
		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
